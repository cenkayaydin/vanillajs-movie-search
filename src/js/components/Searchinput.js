import axios from "axios";
import mixin from '../../services/mixin';

let $ = require('jquery');

var view;
export var films = [];
let Searchinput = {
    render: async () => {
        view = `<div class="input-group mb-3 mt-3">
                    <input type="text" class="form-control" id="myInput" onclick="this.value='';" placeholder="Bulmak istiğiniz filmin adını yazınız">
                    <div class="input-group-append">
                        <i style="padding-left: 5px; padding-top: 5px; padding-right: 10px">${window.location.hash ? JSON.parse(window.localStorage.getItem('films')).length + ' film bulundu' : '' }<img src="${ !window.localStorage.getItem('films') || JSON.parse(window.localStorage.getItem('films')).length < 1 ? 'https://img.icons8.com/ios/50/000000/search-filled.png' : !window.location.hash ? 'https://img.icons8.com/ios/50/000000/right-filled.png': '' }" height="20" width="20" alt=""></i>
                     </div>
                </div>`;
        return view
    },
    after_render: async () => {

        $(document).ready(function () {
            if (window.localStorage.getItem('films')) {
                $('.search').addClass('wait');
                $('.next-button').removeClass('hide');
                if (!window.location.hash) {
                    $('#myInput').val(window.localStorage.getItem('value'));
                    $('.search').removeClass('bottom');
                }
                else {
                    $('#myInput').val(window.localStorage.getItem('value') + ' için Sonuçlar');
                    $('.search').addClass('bottom');
                    $('#myInput').css({color: 'blue'})
                }
            }
            $("#myInput").on("keyup", function () {
                var value = $('#myInput').val().toLowerCase();
                axios.get('https://www.omdbapi.com/?&apikey=24dcb4db&s=' + value + '&type=movie')
                    .then(function (response) {
                        if (response.data.Response === 'False') {
                            films = [];
                            window.localStorage.setItem('films', JSON.stringify(Object.values(films)));
                        } else {
                            $.each(response.data.Search, function (index, movie) {
                                if (mixin.sourceKey(films, movie.imdbID)) {
                                } else {
                                    const data = {
                                        id: movie.imdbID,
                                        title: movie.Title,
                                        poster: movie.Poster,
                                    };
                                    films[movie.imdbID] = data;
                                    window.localStorage.setItem('films', JSON.stringify(Object.values(films)));
                                    window.localStorage.setItem('value', value);
                                    window.location.reload();
                                }
                            });
                        }
                    })
                    .catch(function (error) {
                        console.log(error)
                    });
            });
        });
    }
};

export default Searchinput;