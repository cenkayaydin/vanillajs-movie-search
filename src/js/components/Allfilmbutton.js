let $ = require('jquery');

var view;
export var films = [];
let Allfilmbutton = {
    render: async () => {
        var val = window.localStorage.getItem('value');
        if (!window.location.hash) {
            view = `<div class="next-button hide" id="allFilm" style="text-align: center"><a href="#/p/${val}">daha fazla göster >></a></div>`;
        } else {
            view = `<div class="next-button allfilm p-3 hide" id="topCont" style="text-align: center">Başa dön</div>`;
        }

        return view
    },
    after_render: async () => {
        $('#topCont').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        })
    }
};

export default Allfilmbutton;