import "../css/style.less"
import 'bootstrap/dist/css/bootstrap.min.css'
import Home from "./pages/Home.js"
import Searchinput from "./components/Searchinput.js"
import Allfilmbutton from "./components/Allfilmbutton.js"
import Allfilm from "./pages/Allfilm.js"
import Utils from "../services/utils.js"

const routes = {
    '/': Home,
    '/p/:id': Allfilm
};

const router = async () => {
    const search = null || document.getElementById('search');
    const button = null || document.getElementById('AllFilm');
    const content = null || document.getElementById('conta');

    search.innerHTML = await Searchinput.render();
    await Searchinput.after_render();

    button.innerHTML = await Allfilmbutton.render();
    await Allfilmbutton.after_render();

    let request = Utils.parseRequestUrl();

    let parseUrl = (request.resource ? '/' + request.resource : '/') + (request.id ? '/:id' : '') + (request.verb ? '/' + request.verb : '');

    let page = routes[parseUrl] ? routes[parseUrl]: console.log('404');

    content.innerHTML = await page.render();
    await page.after_render();
};

window.addEventListener('hashchange', router);
window.addEventListener('load', router);

