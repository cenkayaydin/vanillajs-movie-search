let $ = require('jquery');

var view = '';
var all_film = {};
let Allfilm = {
    render: async () => {
        all_film = JSON.parse(window.localStorage.getItem('films'));
        $.each(all_film, function (index, film) {
            view += `
                        <div class="card inline-c" style="max-width: 540px;">
                          <div class="row no-gutters ${film.poster === 'N/A' ? 'bg' : ''}">
                            <div class="col-md-4">
                              <img src="${film.poster}" class="card-img" alt="">
                            </div>
                            <div class="col-md-8">
                              <div class="card-body">
                                <h5 class="card-title">${film.title.toLowerCase()}</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                              </div>
                            </div>
                          </div>
                        </div>
                    `;
        });
        return view
    },
    after_render: async () => {
        var val = window.localStorage.getItem('value').toLowerCase();
        $('.card-title:contains("' + val + '")').each(function () {
            $(this).html($(this).html().replace(val, "<span class='search-key'>" + val + "</span>"));
        });
    },
};

export default Allfilm;
