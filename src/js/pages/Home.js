let $ = require('jquery');

var view = '';
var film_list = {};
let Home = {
    render: async () => {
        film_list = JSON.parse(window.localStorage.getItem('films'));
        $.each(film_list, function (index, film) {
            view += `<div class="col-sm-6 offset-sm-3"><div class="card">
                        <div class="raw no-gutters ${film.poster === 'N/A' ? 'bg' : ''}">
                            <div class="col-md-4 float-left">
                                <img class="card-img" src="${film.poster}" alt="">
                            </div>
                            <div class="col-md-8 float-right">
                            <div class="card-body">
                                <h5 class="card-title">${film.title.toLowerCase()}</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                            </div>
                        </div>
                    </div></div>`
        });
        return view
    },
    after_render: async () => {
        var val = window.localStorage.getItem('value').toLowerCase();
        if (!window.location.hash) {
            $(".card").slice(2).css("display", "none");
        }
        $('.card-title:contains("' + val + '")').each(function () {
            $(this).html($(this).html().replace(val, "<span class='search-key'>" + val + "</span>"));
        });
    },
};

export default Home;
