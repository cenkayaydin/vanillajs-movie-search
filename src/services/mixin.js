const mixin = {
    sourceKey: function (source, key) {
        if (key in source) {
            return source[key]
        }
        return false;
    }
}

export default mixin