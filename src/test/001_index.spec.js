
/* EcmaScriptten kaynaklı hata alıyorum ve yeterli zamanım olmadığı için düzeltemedim bu nedenle test çalışmıyor fakat kod doğrudur. */

const chaiHttp = require('chai-http');
const chai = require('chai');
chai.use(require('chai-dom'));
chai.use(chaiHttp);

import MockAdapter from "axios-mock-adapter";

import axios from "axios";
import Home from "../js/pages/Home"
import Search from "../js/components/Searchinput"
import Allfilmbutton from '../js/components/Allfilmbutton'

var response = {
    Search: [
        {
            Title: 'Abc',
            Poster: 'abc.jpg',
            imdbID: 'abcdef'
        },
        {
            Title: 'Abc1',
            Poster: 'abc1.jpg',
            imdbID: 'abcdef1'
        }
    ]
};

describe('Search Testing', function () {
    var mock = new MockAdapter(axios);
    var search = Search;
    mock = new MockAdapter(axios, {delayResponse: 0});
    beforeEach(function () {
        var val = "abc";
        search.find("input").element.value = val;
        search.find("input").trigger("input");
        mock.onGet('https://www.omdbapi.com/?&apikey=24dcb4db&s=' + val + '&type=movie').replyOnce(200, response);
    });
    afterEach(function () {
        mock.reset();
    });

    it('Dönen response card içerisinde doğru yerlere yerleşiyor mu ?', function (done) {
        var home = Home;
        setTimeout(function () {
            expect(home.find('h5.card-title').element.textContent).to.equal(response.Search[0].Title);
            expect(home.find('img.card-img').attribute('src')).to.equal(response.Search[0].Poster);
            done()
        }, 20)
    });

    it('Home ekranında yüklenen card sayıları ikinin altında mı ?', function (done) {
        var home = Home;
        setTimeout(function () {
            expect(home.findAll('.col-sm-6').length()).to.equal(2);
            done()
        }, 20)
    });

    it('Kullanıcı daha fazla sonuç göster dediğinde hash değişiyor mu ve hash doğru yapıya ulaşıyor mu ?', function (done) {
        var button = Allfilmbutton;
        var value = window.localStorage.getItem('value');
        expect(window.location.hash).to.equal('/');
        button.find('next-button').trigger('click');
        setTimeout(function () {
            expect(window.location.hash).to.equal('/#/p/' + value);
            done()
        }, 20)
    })
});

