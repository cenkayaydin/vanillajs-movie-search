module.exports = {
    entry: './src/js/app.js',
    output: {
        filename: "bundle.js",
        path: __dirname + '/dist',
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!less-loader'
            },
            {
                test: /\.css$/,

                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader'
                    }
                ]
            },
        ]
    }
};